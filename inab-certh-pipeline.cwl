#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: Workflow

requirements:
  - class: StepInputExpressionRequirement
  - class: InlineJavascriptRequirement
  - class: MultipleInputFeatureRequirement


inputs:
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # trim_galore
 
  trim_galore_file1: 
    type: File

  trim_galore_file2:  
    type: File

  trim_galore_paired:  
    type: boolean


  # minimap2

  minimap2_threads:
    type: int

  minimap2_target:
    type: File

  minimap2_preset:
    type:
      - 'null'
      - type: enum
        symbols:
          - map-pb
          - map-ont
          - ava-pb
          - ava-ont
          - asm5
          - asm10
          - asm20
          - splice
          - sr



  minimap2_output_name:
    type: string

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # samtools

  samtools_threads:
    type: int

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # samtools-view-1

  samtools_view_1_isbam:
    type: boolean

  samtools_view_1_output_name:
    type: string

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # samtools-flagstat-1

  samtools_flagstat_1_output_name:
    type: string

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # samtools-sort-1

  samtools_sort_1_sort_by_name:
    type: boolean

  samtools_sort_1_output_name:
    type: string
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # samtools-view-2

  samtools_view_2_uncompressed:
    type: boolean

  samtools_view_2_readswithbits:
    type: int

  samtools_view_2_readswithoutbits:
    type: int

  samtools_view_2_output_name:
    type: string

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # samtools-sort-2

  samtools_sort_2_output_name:
    type: string

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # printf

  printf_message:
    type: string

  printf_output_name:
    type: string

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # samtools-view-3 

  samtools_view_3_count:
    type: boolean

  samtools_view_3_output_name:
    type: string

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # samtools-index-1

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # bedtools-coverage 

  bedtools_coverage_A_file:
    type: File


  bedtools_coverage_per_base:
    type: boolean

  bedtools_coverage_output_name:
    type: string

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # picard-mark-duplicates

  picard_mark_duplicates_output_name:
    type: string

  picard_mark_duplicates_metrics_name:
    type: string

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # samtools-flagstat-2

  samtools_flagstat_2_output_name:
    type: string

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # picard-add-or-replace

  picard_add_or_replace_rgid:
    type: int

  picard_add_or_replace_rglb:
    type: string

  picard_add_or_replace_rgpl:
    type: string

  picard_add_or_replace_rgpu:
    type: string

  picard_add_or_replace_rgsm:
    type: string

  picard_add_or_replace_output_name:
    type: string

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # samtools-index-2


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # samtools-faidx-1

  fasta:
    type: File

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # picard-create-sequence-dictionary-1
  picard-create-sequence-dictionary_output_name:
    type: string


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # gatk-haplotype-caller


  gatk_haplotype_caller_min_base:
    type: int

  gatk_haplotype_caller_output_name:
    type: string


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # gatk-index-feature-file-1

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # gatk-variant-filtration


  gatk_variant_filtration_filter_expression:
    type: string

  gatk_variant_filtration_filter_name:
    type: string

  gatk_variant_filtration_output_name:
    type: string


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # gatk-select-variants


  gatk_select_variants_exclude_filtered:
    type: boolean

  gatk_select_variants_output_name:
    type: string


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # bcftools-consensus-1

  bcftools_consensus_1_output_name:
    type: string

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # sed


  sed_regex:
    type: string

  sed_output_name:
    type: string







outputs:
  bedtools-coverage:
    type: File
    outputSource: bedtools-coverage/output

  bcftools_consensus_1_out:
    type: File
    outputSource: bcftools-consensus-1/output

  sed_out:
    type: File
    outputSource: sed/output

  r1_out:
    type: File
    outputSource: rscript_1/output
  r2_out:
    type: File
    outputSource: rscript_2/output
  r3_out:
    type: File
    outputSource: rscript_3/output




#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

steps:

## Adaptor trimming ##
  # trim_galore --path_to_cutadapt cutadapt -o trimmedData/ --paired sampleID_R1_001.fastq.gz sampleID_R2_001.fastq.gz > sampleID_trimming.output 2>&1
  trim_galore:
    run: ./tools/trim_galore/trim_galore.cwl
    in: 
      fastq1: trim_galore_file1
      fastq2: trim_galore_file2
      paired: trim_galore_paired
    out: [clfastq1,clfastq2,clstats1,clstats2]


  cat:
    run: ./tools/cat/cat.cwl
    in: 
      trimming_report1: trim_galore/clstats1
      trimming_report2: trim_galore/clstats2
    out: [output]


  ## Initial mapping ##
  # minimap2 -t 16 -ax sr NC_045512.fasta trimmedData/sampleID_R1_001_val_1.fq.gz trimmedData/sampleID_R2_001_val_2.fq.gz > sampleID.sam
  minimap2:
    run: ./tools/minimap2/minimap2.cwl
    in: 
      target: minimap2_target
      threads: minimap2_threads
      preset: minimap2_preset
      query: [trim_galore/clfastq1,trim_galore/clfastq2]
      output_name: minimap2_output_name
    out: [output]


  # samtools view -@ 16 -Sb sampleID.sam > sampleID.bam
  samtools-view-1:
    run: ./tools/samtools/samtools-view.cwl
    in: 
      input: minimap2/output
      threads: samtools_threads
      isbam: samtools_view_1_isbam
      output_name: samtools_view_1_output_name
    out: [output]


  # samtools flagstat -@ 16 sampleID.bam > sampleID_rawInput_report.out
  samtools-flagstat-1:
    run: ./tools/samtools/samtools-flagstat.cwl
    in:
      input: samtools-view-1/output
      threads: samtools_threads
      output_name: samtools_flagstat_1_output_name
    out: [output]


  # samtools sort -@ 16 -n sampleID.bam -o sampleID_sorted.bam
  samtools-sort-1:
    run: ./tools/samtools/samtools-sort.cwl
    in: 
      input: samtools-view-1/output
      threads: samtools_threads
      sort_by_name: samtools_sort_1_sort_by_name
      output_name: samtools_sort_1_output_name
    out: [sorted]

  # samtools view -@ 16 -u -f 1 -F 12 sampleID_sorted.bam > sampleID_mapped.bam
  samtools-view-2:
    run: ./tools/samtools/samtools-view.cwl
    in: 
      input: samtools-sort-1/sorted
      threads: samtools_threads
      uncompressed: samtools_view_2_uncompressed
      readswithbits: samtools_view_2_readswithbits
      readswithoutbits: samtools_view_2_readswithoutbits
      output_name: samtools_view_2_output_name
    out: [output]


  # samtools sort -@ 16 sampleID_mapped.bam -o sampleID_sorted_mapped.bam
  samtools-sort-2:
    run: ./tools/samtools/samtools-sort.cwl
    in: 
      input: samtools-view-2/output
      threads: samtools_threads
      output_name: samtools_sort_2_output_name
    out: [sorted]

  # printf 'sampleID	' >> sorted_mapped_flagstat.txt
  printf:
    run: ./tools/printf/printf.cwl
    in: 
      message: printf_message
      output_name: printf_output_name
    out: [output]


  # samtools view -c sampleID_sorted_mapped.bam >> sorted_mapped_flagstat.txt
  samtools-view-3:
    run: ./tools/samtools/samtools-view.cwl
    in: 
      input: samtools-sort-2/sorted
      count: samtools_view_3_count
      output_name: samtools_view_3_output_name
    out: [output]


  # samtools index sampleID_sorted_mapped.bam
  samtools-index-1:
    run: ./tools/samtools/samtools-index.cwl
    in: 
      bam_sorted: samtools-sort-2/sorted
    out: [bam_sorted_indexed]


  ## Calculate coverage ##
  # bedtools coverage -a GCF_009858895.2_ASM985889v3_genomic.gff -b sampleID_sorted_mapped.bam -d > sampleID_coverage_perbase.txt
  bedtools-coverage:
    run: ./tools/bedtools/bedtools-coverage.cwl
    in: 
      A_file: bedtools_coverage_A_file
      B_file: samtools-sort-2/sorted
      per_base: bedtools_coverage_per_base
      output_name: bedtools_coverage_output_name
    out: [output]


  # java -jar picard.jar MarkDuplicates I=sampleID_sorted_mapped.bam O=sampleID_sorted_uniq.bam M=sampleID_dup_metrics.txt
  picard-mark-duplicates:
    run: ./tools/picard/picard-mark-duplicates.cwl
    in: 
      input: samtools-sort-2/sorted
      output_name: picard_mark_duplicates_output_name
      metrics_name: picard_mark_duplicates_metrics_name
    out: [output]


  # samtools flagstat -@ 16 sampleID_sorted_uniq.bam > sampleID_mapping_report.out
  samtools-flagstat-2:
    run: ./tools/samtools/samtools-flagstat.cwl
    in: 
      input: picard-mark-duplicates/output
      threads: samtools_threads
      output_name: samtools_flagstat_2_output_name
    out: [output] 


  ## Identify variants using GATK HaplotypeCaller ##
  # java -jar picard.jar AddOrReplaceReadGroups I=../sampleID_sorted_uniq.bam O=sampleID_sorted_uniq_rg.bam RGID=1 RGLB=lib1 RGPL=illumina RGPU=unit1 RGSM=sampleID
  picard-add-or-replace:
    run: ./tools/picard/picard-add-or-replace.cwl
    in: 
      input: picard-mark-duplicates/output
      rgid: picard_add_or_replace_rgid
      rglb: picard_add_or_replace_rglb
      rgpl: picard_add_or_replace_rgpl
      rgpu: picard_add_or_replace_rgpu
      rgsm: picard_add_or_replace_rgsm
      output_name: picard_add_or_replace_output_name
    out: [output]


  # samtools index sampleID_sorted_uniq_rg.bam
  samtools-index-2:
    run: ./tools/samtools/samtools-index.cwl
    in: 
      bam_sorted: picard-add-or-replace/output
    out: [bam_sorted_indexed]



  # java -jar picard.jar CreateSequenceDictionary R=NC_045512.fasta O=NC_045512.dict
  picard-create-sequence-dictionary:
    run: ./tools/picard/picard-create-sequence-dictionary.cwl
    in:
      input: fasta
    out: [output]



  # samtools faidx NC_045512.fasta
  samtools-faidx-1:
    run: ./tools/samtools/samtools-faidx.cwl
    in:
      fasta: fasta
      dict: picard-create-sequence-dictionary/output
    out: [fasta_indexed]


  # gatk HaplotypeCaller -R NC_045512.fasta -I sampleID_sorted_uniq_rg.bam --output sampleID_sars_variants.vcf.gz --min-base-quality-score 10
  gatk-haplotype-caller:
    run: ./tools/gatk/gatk-haplotype-caller.cwl
    in: 
      input: samtools-index-2/bam_sorted_indexed
      reference: samtools-faidx-1/fasta_indexed
      min_base: gatk_haplotype_caller_min_base
      output_name: gatk_haplotype_caller_output_name
    out: [output]


  gatk-index-1:
    run: ./tools/gatk/gatk-index-feature-file.cwl
    in:
      featurefile: gatk-haplotype-caller/output
    out: [tbi]

  ## Filter for variants with AF equal or greater than 0.9 ##
  # gatk VariantFiltration -R NC_045512.fasta -V sampleID_sars_variants.vcf.gz -O sampleID_sars_variants_filtered.vcf.gz --filter-expression 'AF < 0.9' --filter-name 'A>
  gatk-variant-filtration:
    run: ./tools/gatk/gatk-variant-filtration.cwl
    in:
      index: gatk-index-1/tbi
      indels_vcf: gatk-haplotype-caller/output
      reference: samtools-faidx-1/fasta_indexed
      filter_expression: gatk_variant_filtration_filter_expression
      filter_name: gatk_variant_filtration_filter_name
      output_name: gatk_variant_filtration_output_name
    out: [output]


  gatk-index-2:
    run: ./tools/gatk/gatk-index-feature-file.cwl
    in:
      featurefile: gatk-variant-filtration/output
    out: [tbi]


    # gatk SelectVariants -R NC_045512.fasta -V sampleID_sars_variants_filtered.vcf.gz -O sampleID_sars_variants_filtered_clean.vcf.gz --exclude-filtered
  gatk-select-variants:
    run: ./tools/gatk/gatk-select-variants.cwl
    in: 
      index: gatk-index-2/tbi
      raw_vcf: gatk-variant-filtration/output
      reference: samtools-faidx-1/fasta_indexed
      exclude_filtered: gatk_select_variants_exclude_filtered
      output_name: gatk_select_variants_output_name
    out: [output]


  gatk-index-3:
    run: ./tools/gatk/gatk-index-feature-file.cwl
    in:
      featurefile: gatk-select-variants/output
    out: [tbi]


  ## Create consensus sequence ##
  # bcftools consensus -f NC_045512.fasta -o sampleID_consensus.fasta sampleID_sars_variants_filtered_clean.vcf.gz
  bcftools-consensus-1:
    run: ./tools/bcftools/bcftools-consensus.cwl
    in: 
      index: gatk-index-3/tbi
      vcf: gatk-select-variants/output
      reference: samtools-faidx-1/fasta_indexed
      output_name: bcftools_consensus_1_output_name
    out: [output]


  # sed 's/^>.*/>sampleID/' sampleID_consensus.fasta > sampleID_consensus_withheader.fasta
  sed:
    run: ./tools/sed/sed-substitute.cwl
    in: 
      input: bcftools-consensus-1/output
      regex: sed_regex
      output_name: sed_output_name
    out: [output]



  ## Three custom R scripts for plotting coverage, and creating read count reporting per sorted_mapped_flagstat ##
  # Rscript mergeCoverage.R
  rscript_1:
    run: ./tools/rscript/mergeCoverage.cwl
    in:
      cover: bedtools-coverage/output
    out: [output]


  rscript_2:
    run: ./tools/rscript/createCoveragePlots.cwl
    in:
      all_samples: rscript_1/output
    out: [output]



  rscript_3:
    run: ./tools/rscript/mergeReports.cwl
    in:
      all_samples: rscript_1/output
      trimming_output: cat/output
      rawInput_report: samtools-flagstat-1/output
      mapping_report: samtools-flagstat-2/output
      sorted_mapped_flagstat: printf/output
    out: [output]
