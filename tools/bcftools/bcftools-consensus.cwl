
#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
- $import: bcftools-docker.yml
- class: InlineJavascriptRequirement
- class:  InitialWorkDirRequirement
  listing:
    - $(inputs.index)
    - $(inputs.vcf)



baseCommand: ["bcftools", "consensus"]


inputs:
  index:
    type: File

  vcf:
    type: File
    inputBinding:
      position: 10

  reference:
    type: File
    inputBinding:
      position: 1
      prefix: -f
    doc: reference sequence in fasta format

  haplotype:
    type: int?
    inputBinding:
      position: 1
      prefix: -H
    doc: apply variants for the given haplotype <1|2>

  mask:
    type: File?
    inputBinding:
      position: 1
      prefix: -m
    doc: replace regions with N

  sample:
    type: string?
    inputBinding:
      position: 1
      prefix: -s
    doc: apply variants of the given sample

  iupac_codes:
    type: boolean?
    inputBinding:
      position: 1
      prefix: -i
    doc: output variants in the form of IUPAC ambiguity codes

  chain:
    type: string?
    inputBinding:
      position: 1
      prefix: -c
    doc: write a chain file for liftover

  output_name:
    type: string
    inputBinding:
      position: 2
      prefix: -o
    doc: write output to a file

outputs:
  output:
    type: File
    outputBinding:
      glob: $(inputs.output_name)

