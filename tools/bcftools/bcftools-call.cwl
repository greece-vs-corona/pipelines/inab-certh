#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
- $import: bcftools-docker.yml
- class: InlineJavascriptRequirement

baseCommand: ["bcftools", "call"]

inputs:
  input:
    type: File
    inputBinding:
      position: 10

  ploidy:
    type: int?
    inputBinding:
      position: 4
      prefix: --ploidy
    doc: | 
      predefined ploidy, use list (or any other unused word) to print a 
      list of all predefined assemblies. Append a question mark to print 
      the actual definition.

  variants_only:
    type: boolean?
    inputBinding:
      position: 1
      prefix: -v
    doc: |
      output variant sites only

  multiallelic_caller:
    type: boolean?
    inputBinding:
      position: 2
      prefix: -m
    doc: |
      alternative model for multiallelic and rare-variant calling designed 
      to overcome known limitations in -c calling model (conflicts with -c)

  output_type:
    type: string
    default: "z"
    inputBinding:
      position: 3
      prefix: -O
    doc: | 
      --output-type b|u|z|v 
      output compressed BCF (b), uncompressed BCF (u), 
      compressed VCF (z), uncompressed VCF (v)

  output_name:
    type: string
    inputBinding:
      position: 9
      prefix: -o
    doc: write output to a file

outputs:
  output:
    type: File
    outputBinding:
      glob: $(inputs.output_name)


