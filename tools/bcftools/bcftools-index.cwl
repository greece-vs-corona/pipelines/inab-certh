#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

hints:
  - $import: bcftools-docker.yml

requirements:
  InitialWorkDirRequirement:
    listing: 
      - $(inputs.vcf)

baseCommand: ["bcftools", "index"]

arguments:
  - valueFrom: -c  # generate CSI-format index for VCF/BCF files [default]
    position: 1

inputs:
  vcf:
    type: File
    inputBinding:
      position: 2

outputs:
  output: 
    type: File
    secondaryFiles: .csi
    outputBinding:
      glob: $(inputs.vcf.basename)
  
