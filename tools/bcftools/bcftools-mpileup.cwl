#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

requirements:
- $import: bcftools-docker.yml
- class: InlineJavascriptRequirement

baseCommand: ["bcftools", "mpileup"]

inputs:
  input:
    type: File
    inputBinding:
      position: 10

  reference:
    type: File
    inputBinding:
      position: 9
      prefix: -f
    doc: reference sequence in fasta format

  output_type:
    type: string
    inputBinding:
      position: 2
      prefix: -O
    doc: | 
      --output-type b|u|z|v 
      output compressed BCF (b), uncompressed BCF (u), 
      compressed VCF (z), uncompressed VCF (v)

  output_name:
    type: string
    inputBinding:
      position: 3
      prefix: -o
    doc: write output to a file

outputs:
  output:
    type: File
    outputBinding:
      glob: $(inputs.output_name)


