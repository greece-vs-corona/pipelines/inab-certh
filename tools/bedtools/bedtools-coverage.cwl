cwlVersion: v1.0
class: CommandLineTool

# This is only a partial/workflow-specific implementation for the 'bedtools' command

hints:
  #- $import: envvar-global.yml
  - $import: bedtools-docker.yml
  - class: InlineJavascriptRequirement

inputs:

  A_file:
    type: File
    inputBinding:
      position: 1
      prefix: -a
    doc: |
      BAM/BED/GFF/VCF file “A”. Each feature in A is compared to B in search of overlaps.;
      Use “stdin” if passing A with a UNIX pipe.

  B_file:
    type: File
    inputBinding:
      position: 2
      prefix: -b
    doc: |
      One or more BAM/BED/GFF/VCF file(s) “B”. Use “stdin” if passing B with a UNIX pipe.;
      NEW!!!: -b may be followed with multiple databases and/or wildcard (*) character(s).

  per_base:
    type: boolean
    default: false
    inputBinding:
      position: 3
      prefix: -d
    doc: | 
      Report the depth at each position in each A feature. Positions reported are one based. 
      Each position and depth follow the complete A feature.

  output_name:
    type: string

stdout: $(inputs.output_name)

outputs:
  output:
    type: File
    outputBinding:
      glob: $(inputs.output_name)

baseCommand: ["bedtools", "coverage"]

