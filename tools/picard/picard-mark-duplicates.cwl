cwlVersion: v1.0
class: CommandLineTool

hints:
- $import: picard-docker.yml
- class: InlineJavascriptRequirement

inputs:
  input:
    type: File
    inputBinding:
      position: 4
      prefix: "I="
      separate: false
    doc: |
      The BAM or SAM file to sort. Required

  output_name:
    type: string
    inputBinding:
      position: 5
      prefix: "O="
      separate: false
    doc: |
      The sorted BAM or SAM output file. Required

  metrics_name:
    type: string
    inputBinding:
      position: 6
      prefix: "M="
      separate: false

outputs:
  output:
    type: File
    outputBinding:
      glob: $(inputs.output_name)

  metrics:
    type: File
    format: edam:format_1964  # TXT
    outputBinding:
      glob: $(inputs.metrics_name)

arguments:

- valueFrom: "/usr/local/bin/picard.jar"
  position: 2
  prefix: "-jar"

- valueFrom: "MarkDuplicates"
  position: 3

baseCommand: ["java"]

