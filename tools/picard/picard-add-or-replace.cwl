cwlVersion: v1.0
class: CommandLineTool

hints:
- $import: picard-docker.yml
- class: InlineJavascriptRequirement

inputs:
  java_arg:
    type: string
    default: "-Xmx2g"
    inputBinding:
      position: 1

  input:
    type: File
    inputBinding:
      position: 4
      prefix: "I="
      separate: false
    doc: |
      The BAM or SAM file to sort. Required

  output_name:
    type: string
    inputBinding:
      position: 5
      prefix: "O="
      separate: false
    doc: |
      The sorted BAM or SAM output file. Required

  rgid:
    type: int
    inputBinding:
      position: 6
      prefix: "RGID="
      separate: false

  rglb:
    type: string
    inputBinding:
      position: 7
      prefix: "RGLB="
      separate: false

  rgpl:
    type: string
    inputBinding:
      position: 8
      prefix: "RGPL="
      separate: false

  rgpu:
    type: string
    inputBinding:
      position: 9
      prefix: "RGPU="
      separate: false

  rgsm:
    type: string
    inputBinding:
      position: 10
      prefix: "RGSM="
      separate: false

outputs:
  output:
    type: File
    outputBinding:
      glob: $(inputs.output_name)

arguments:

- valueFrom: "/usr/local/bin/picard.jar"
  position: 2
  prefix: "-jar"

- valueFrom: "AddOrReplaceReadGroups"
  position: 3

- valueFrom: "true"
  position: 11
  separate: false
  prefix: CREATE_INDEX=

baseCommand: ["java"]

