#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

hints:
  - $import: rscript-docker.yml
  - class: InlineJavascriptRequirement



requirements:
  InitialWorkDirRequirement:
    listing: 
      - $(inputs.cover)

baseCommand: ["Rscript", "/usr/local/src/scripts/mergeCoverage.R"]

inputs:
  cover:
    type: File

outputs:
  output:
    type: File
    outputBinding:
      glob: coverageAllSamples.txt
 

