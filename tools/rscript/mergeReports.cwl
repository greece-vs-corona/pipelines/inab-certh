#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

hints:
  - $import: rscript-docker.yml
  - class: InlineJavascriptRequirement



requirements:
  InitialWorkDirRequirement:
    listing: 
      - $(inputs.trimming_output)
      - $(inputs.rawInput_report)
      - $(inputs.mapping_report)
      - $(inputs.sorted_mapped_flagstat)
      - $(inputs.all_samples)

baseCommand: ["Rscript", "/usr/local/src/scripts/mergeReports.R"]

inputs:

  trimming_output:
    type: File

  rawInput_report:
    type: File

  mapping_report:
    type: File

  sorted_mapped_flagstat:
    type: File

  all_samples:
    type: File


outputs:
  output:
    type: File
    outputBinding:
      glob: allSamples-reports.csv
 

