#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

hints:
  - $import: rscript-docker.yml
  - class: InlineJavascriptRequirement



requirements:
  InitialWorkDirRequirement:
    listing: 
      - $(inputs.all_samples)

baseCommand: ["Rscript", "/usr/local/src/scripts/createCoveragePlots.R"]

inputs:
  all_samples:
    type: File

outputs:
  output:
    type: File
    outputBinding:
      glob: coveragePlots.pdf
 

