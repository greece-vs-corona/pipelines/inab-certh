  # trim_galore --paired sampleID_R1_001.fastq.gz sampleID_R2_001.fastq.gz > sampleID_trimming.output 2>&1


class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  dct: 'http://purl.org/dc/terms/'
  doap: 'http://usefulinc.com/ns/doap#'
  foaf: 'http://xmlns.com/foaf/0.1/'
  sbg: 'https://www.sevenbridges.com/'
id: trim_galore_0_6_2
baseCommand:
  - trim_galore
inputs:
  - id: fastq1
    type: File
    inputBinding:
      position: 2
    doc: READ1 of the paired-end run
  - id: fastq2
    type: File
    inputBinding:
      position: 3
    doc: READ2 of the pair-end run
  - id: paired
    type: boolean
    inputBinding:
      position: 1
      prefix: '--paired'


outputs:
  - id: clfastq1
    type: File
    outputBinding:
      glob: '$(inputs.fastq1.basename.replace(''.fastq.gz'', ''_val_1.fq.gz''))'
  - id: clfastq2
    type: File
    outputBinding:
      glob: '$(inputs.fastq2.basename.replace(''.fastq.gz'', ''_val_2.fq.gz''))'
  - id: clstats1
    type: File
    outputBinding:
      glob: >-
        $(inputs.fastq1.basename.replace('.fastq.gz',
        '.fastq.gz_trimming_report.txt'))
  - id: clstats2
    type: File
    outputBinding:
      glob: >-
        $(inputs.fastq2.basename.replace('.fastq.gz',
        '.fastq.gz_trimming_report.txt'))
label: trim_galore_0.6.2
requirements:
  - class: ResourceRequirement
    ramMin: 8000
    coresMin: 4
  - class: DockerRequirement
    dockerPull: 'mskaccess/trim_galore:0.6.3'
  - class: InlineJavascriptRequirement
