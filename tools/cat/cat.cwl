class: CommandLineTool
cwlVersion: v1.0
baseCommand: cat

stdout: sampleID_trimming.output
inputs:
  trimming_report1:
    type: File
    inputBinding:
      position: 1
  trimming_report2:
    type: File
    inputBinding:
      position: 2

outputs:
  output:
    type: stdout
