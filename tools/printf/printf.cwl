cwlVersion: v1.0
class: CommandLineTool

requirements:
- class: InlineJavascriptRequirement

inputs:
  message:
    type: string
    inputBinding:
      position: 1

  output_name: 
    type: string

stdout: $(inputs.output_name)

outputs:
  output:
    type: File
    outputBinding:
      glob: $(inputs.output_name)

baseCommand: [printf]

