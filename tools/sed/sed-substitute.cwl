#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool

requirements:
- class: InlineJavascriptRequirement

baseCommand: [sed]

inputs:
  input:
    type: File
    inputBinding:
      position: 10

  regex:
    type: string
    inputBinding:
      position: 1 

  output_name: 
    type: string

stdout: $(inputs.output_name)

outputs:
  output:
    type: File
    outputBinding:
      glob: $(inputs.output_name)

