#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

hints:
  - $import: samtools-docker.yml

requirements:
  InitialWorkDirRequirement:
    listing: 
      - $(inputs.fasta)
      - $(inputs.dict)

baseCommand: ["samtools", "faidx"]

inputs:
  fasta:
    type: File
    inputBinding:
      position: 1
  dict:
    type: File

outputs:
  fasta_indexed:
    type: File
    secondaryFiles: [.fai,^.dict]
    outputBinding:
      glob: $(inputs.fasta.basename)

 

