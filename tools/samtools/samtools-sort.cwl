#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

hints:
- $import: samtools-docker.yml
- class: InlineJavascriptRequirement

baseCommand: ["samtools", "sort"]

inputs:
  threads:
    type: int?
    inputBinding:
      prefix: -@

  input:
    type: File
    inputBinding:
      position: 5

  output_name:
    type: string
    inputBinding:
      prefix: -o
      position: 10
    doc: Desired output filename.

  sort_by_name:
    type: boolean?
    inputBinding:
      prefix: -n
    doc: Sort by read names (i.e., the QNAME field) rather than by chromosomal coordinates.

outputs:
  sorted:
    type: File
    format: edam:format_2572  # BAM
    outputBinding:
      glob: $(inputs.output_name)



