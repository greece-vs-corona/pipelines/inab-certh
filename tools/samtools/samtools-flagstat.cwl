#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

hints:
- $import: samtools-docker.yml
- class: InlineJavascriptRequirement

baseCommand: ["samtools", "flagstat"]

inputs:
  input:
    type: File
    inputBinding:
      position: 4

##  flagstat: invalid option -- '@'  ##
#  threads:
#    type: int?
#    inputBinding:
#      position: 1
#      prefix: -@

  output_name:
    type: string

stdout: $(inputs.output_name)

outputs:
  output:
    type: File
    outputBinding:
      glob: $(inputs.output_name)

