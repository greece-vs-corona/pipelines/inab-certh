#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

baseCommand: minimap2

requirements:
- $import: minimap2-docker.yml
- class: InlineJavascriptRequirement

inputs:
  preset:
    type:
      - 'null'
      - type: enum
        symbols:
          - map-pb
          - map-ont
          - ava-pb
          - ava-ont
          - asm5
          - asm10
          - asm20
          - splice
          - sr 
    inputBinding:
      prefix: "-x"

  target:
    type: File
    inputBinding:
      position: 5

  query:
    type: File[]
    inputBinding:
      position: 6

  threads:
    type: int?
    inputBinding:
      position: 1
      prefix: -t

  output_name: 
    type: string

arguments:
  - -a # output in SAM format

stdout: $(inputs.output_name)

outputs:
  output:
    type: File
    outputBinding:
      glob: $(inputs.output_name)


