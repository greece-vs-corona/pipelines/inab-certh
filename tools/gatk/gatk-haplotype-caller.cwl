cwlVersion: v1.0
class: CommandLineTool

# This is only a partial/workflow-specific implementation for the 'HaplotypeCaller' command

hints:
- $import: gatk-docker.yml
- class: InlineJavascriptRequirement

inputs:

  reference:
    type: File
    inputBinding:
        position: 3
        prefix: -R

  input:
    type: File
    inputBinding:
      position: 4
      prefix: -I

  min_base:
    type: int
    inputBinding:
      position: 5
      prefix: --min-base-quality-score

  output_name:
    type: string
    inputBinding:
      position: 6
      prefix: --output


outputs:
  output:
    type: File
    outputBinding:
      glob: $(inputs.output_name)

arguments:

- valueFrom: "HaplotypeCaller"
  position: 1
  # separate: true
  # prefix: "-T"

baseCommand: ["gatk"]

