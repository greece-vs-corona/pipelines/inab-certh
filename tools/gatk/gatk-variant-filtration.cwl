cwlVersion: v1.0
class: CommandLineTool

# This is only a partial/workflow-specific implementation for the 'VariantFiltration' command

hints:
- $import: gatk-docker.yml
- class: InlineJavascriptRequirement

requirements:
  InitialWorkDirRequirement:
    listing:
      - $(inputs.index)
      - $(inputs.indels_vcf)



inputs:

  reference:
    type: File
    inputBinding:
        position: 3
        prefix: -R

  filter_expression:
    type: string
    default: "QD < 2.0 || FS > 200.0 || ReadPosRankSum < -20.0"
    inputBinding:
      position: 7
      prefix: --filter-expression

  filter_name:
    type: string
    default: "indel_filter"
    inputBinding:
      position: 8
      prefix: --filter-name

  indels_vcf:
    type: File
    inputBinding:
      position: 6
      prefix: -V

  output_name:
    type: string
    inputBinding:
      position: 6
      prefix: -O

  index:
    type: File

outputs:
  output:
    type: File
    outputBinding:
      glob: $(inputs.output_name)

arguments:

- valueFrom: "VariantFiltration"
  position: 1
  # separate: true
  # prefix: "-T"

baseCommand: ["gatk"]

