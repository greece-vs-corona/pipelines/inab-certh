#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool


requirements:
    InlineJavascriptRequirement: {}
    DockerRequirement:
        dockerPull: broadinstitute/gatk:4.1.8.0


baseCommand: [ gatk, IndexFeatureFile ]

inputs:
  featurefile:
    type: File
    doc: Input VCF file
    inputBinding:
      prefix: -I


arguments:
  - id: output
    prefix: --output
    valueFrom: $(inputs.featurefile.basename).tbi

outputs:
  tbi:
    type: File
    outputBinding:
      glob: $(inputs.featurefile.basename).tbi
