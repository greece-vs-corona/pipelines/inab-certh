cwlVersion: v1.0
class: CommandLineTool

# This is only a partial/workflow-specific implementation for the 'SelectVariants' command

hints:
- $import: gatk-docker.yml
- class: InlineJavascriptRequirement

requirements:
  InitialWorkDirRequirement:
    listing:
      - $(inputs.index)
      - $(inputs.raw_vcf)


inputs:

  reference:
    type: File
    inputBinding:
        position: 3
        prefix: -R

  exclude_filtered:
    type: boolean
    inputBinding:
      position: 5
      prefix: --exclude-filtered

  raw_vcf:
    type: File
    inputBinding:
      position: 6
      prefix: -V
    doc: input vcf File raw variant calls from haplotype caller

  output_name:
    type: string
    inputBinding:
      position: 6
      prefix: -O

  index:
    type: File

outputs:
  output:
    type: File
    outputBinding:
      glob: $(inputs.output_name)

arguments:

- valueFrom: "SelectVariants"
  position: 1
  # separate: true
  # prefix: "-T"

baseCommand: ["gatk"]

