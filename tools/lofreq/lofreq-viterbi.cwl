#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

hints:
  - $import: lofreq-docker.yml

requirements:
  InitialWorkDirRequirement:
    listing:
      - $(inputs.reference)

baseCommand: ["lofreq", "viterbi"]

inputs:
  input:  
    type: File
    format: edam:format_2572  # BAM
    inputBinding: {}

  reference:
    type: File
    format: edam:format_1929  # FASTA
    inputBinding:
      prefix: --ref

  defqual:
    type: int?
    inputBinding:
      prefix: --defqual

  output_name:
    type: string
    inputBinding:
      position: 1
      prefix: -o

outputs:
  output:
    type: File
    format: edam:format_2572  # BAM
    outputBinding:
      glob: $(inputs.output_name)


