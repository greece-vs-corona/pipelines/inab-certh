#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

hints:
  - $import: lofreq-docker.yml

baseCommand: [lofreq, call-parallel]

inputs:
  input:
    type: File
    format: edam:format_2572  # BAM
    inputBinding:
      position: 1

  reference_fasta:
    type: File
    format: edam:format_1929  # FASTA
    inputBinding:
      prefix: --ref
      position: 3

  threads:
    type: int?
    default: 1
    inputBinding:
      prefix: --pp-threads
      position: 4

  call_indels:
    type: boolean?
    inputBinding:
      prefix: --call-indels
      position: 10
    doc: "Enable indel calls (note: preprocess your file to include indel alignment qualities!)"

  min_cov:
    type: int?
    default: 10
    inputBinding:
      prefix: --min-cov
      position: 11
    doc: "Test only positions having at least this coverage [1] (note: without --no-default-filter default filters (incl. coverage) kick in after predictions are done)"

  verbose:
    type: boolean?
    inputBinding:
      prefix: --verbose
      position: 20

#  max_depth_cov: 1000000 #default
#  min_bq: 6  #default
#  min_alt_bq: 6  #default
#  min_mq: 0  #default
#  max_mapping_quality: 255  #default
#  min_jq: 0  #default
#  min_alt_jq: 0  #default
#  def_alt_jq: 0  #default
#  pvalue_cutoff: 0.010000  #default
#  bonferroni: dynamic  #default

  output_name:
    type: string
    inputBinding:
      prefix: --out
      position: 2

outputs:
  output:
    type: File
    format: edam:format_3016  # VCF
    outputBinding:
      glob: $(inputs.output_name)

