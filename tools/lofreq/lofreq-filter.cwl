#!/usr/bin/env cwl-runner
cwlVersion: v1.0
class: CommandLineTool

hints:
  - $import: lofreq-docker.yml

baseCommand: ["lofreq", "filter"]

inputs:
  input:
    type: File
    format: edam:format_3016  # VCF
    inputBinding:
      position: 1
      prefix: -i

  af_min:
    type: float
    inputBinding:
      prefix: --af-min

  output_name:
    type: string
    inputBinding:
      position: 2
      prefix: -o

outputs:
  output:
    type: File
    format: edam:format_3016  # VCF
    outputBinding:
      glob: $(inputs.output_name)



